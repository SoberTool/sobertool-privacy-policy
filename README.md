# SoberTool Privacy Policy

This repository holds SoberTool's privacy policies for the various platforms that SoberTool is on. Please find the appropriate Privacy Policy for the platform you are using. Thank you for using SoberTool!